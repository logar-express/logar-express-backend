<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('users')->insert([
            'Email' => 'admin@admin.com',
            'Name' => 'Administrator',
            'Password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'Email' => 'user@user.com',
            'Name' => 'Administrator',
            'Password' => bcrypt('123456'),
        ]);
    }
}
