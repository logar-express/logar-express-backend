<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFretesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fretes', function (Blueprint $table) {
            $table->id();
            $table->string('tipo_veiculo');
            $table->string('tipo_carreta')->nullable();
            $table->string('tipo_carroceria');
            $table->string('dimensoes_minimas');
            $table->string('requisitos_extras');
            $table->string('tipo_produto');
            $table->string('acondicionamento_produto');
            $table->string('nome_responsavel_coleta');
            $table->string('data_coleta');
            $table->string('celular_coleta');
            $table->string('endereco_coleta');
            $table->string('cidade_coleta');
            $table->string('estado_coleta');
            $table->float('valor_frete');
            $table->float('peso_toneladas');
            $table->string('nome_responsavel_entrega');
            $table->string('data_entrega');
            $table->string('celular_entrega');
            $table->string('endereco_entrega');
            $table->string('cidade_entrega');
            $table->string('estado_entrega');
            $table->boolean('frete_aprovado');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fretes');
    }
}
