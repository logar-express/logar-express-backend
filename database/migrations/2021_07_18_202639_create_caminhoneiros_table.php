<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaminhoneirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caminhoneiros', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('cpf')->unique();
            $table->string('cnh')->unique();
            $table->string('data_emissao_cnh');
            $table->string('estado_expeditor_cnh');
            $table->string('telefone');
            $table->string('celular');
            $table->boolean('possui_mopp');
            $table->string('marca');
            $table->string('modelo');
            $table->string('cor');
            $table->string('placa');
            $table->string('ano');
            $table->string('renavam');
            $table->string('tipo_veiculo');
            $table->string('tipo_carreta');
            $table->string('tipo_carroceria');
            $table->string('id_usuario');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caminhoneiros');
    }
}
