<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicidades', function (Blueprint $table) {
            $table->id();
            $table->string  ('titulo');
            $table->string  ('segmento');
            $table->float   ('preco');
            $table->string  ('descricao');
            $table->integer ('tempo_dias');
            $table->string  ('caminho_arquivo');
            $table->string  ('codigo_publicidade');
            $table->boolean ('publicidade_aprovada');
            $table->string  ('empresa_id');
            $table->date    ('data_expiracao');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicidades');
    }
}
