<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Frete extends Model
{
    use HasFactory, SoftDeletes;

    protected $hidden = [
        'deleted_at',
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'logo',
        'tipo_veiculo',
        'tipo_carreta',
        'tipo_carroceria',
        'dimensoes_minimas',
        'requisitos_extras',
        'tipo_produto',
        'acondicionamento_produto',
        'nome_responsavel_coleta',
        'data_coleta',
        'celular_coleta',
        'endereco_coleta',
        'cidade_coleta',
        'estado_coleta',
        'valor_frete',
        'peso_toneladas',
        'nome_responsavel_entrega',
        'data_entrega',
        'celular_entrega',
        'endereco_entrega',
        'cidade_entrega',
        'estado_entrega',
        'frete_aprovado',
        'empresa_id',
        'tempo_dias',
        'data_expiracao',
        'codigo_frete'
    ];
}
