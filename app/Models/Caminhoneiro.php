<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caminhoneiro extends Model
{
    use HasFactory, SoftDeletes;

    protected $hidden = [
        'deleted_at',
        'updated_at',
        'created_at',
        'id_usuario',
    ];

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'nome',
        'cpf',
        'cnh',
        'data_emissao_cnh',
        'estado_expeditor_cnh',
        'telefone',
        'celular',
        'possui_mopp',
        'marca',
        'modelo',
        'cor',
        'placa',
        'ano',
        'renavam',
        'tipo_veiculo',
        'tipo_carreta',
        'tipo_carroceria',
        'id_usuario',
    ];
}
