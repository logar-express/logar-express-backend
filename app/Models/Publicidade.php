<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publicidade extends Model
{
    use HasFactory, SoftDeletes;

    protected $hidden = [
        'deleted_at',
        'updated_at',
        'created_at',
        'data_expiracao',
        'empresa_id',
    ];

    protected $dates = [
        'data_expiracao',
        'deleted_at',
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'titulo',
        'segmento',
        'preco',
        'descricao',
        'tempo_dias',
        'caminho_arquivo',
        'codigo_publicidade',
        'publicidade_aprovada',
        'imagem_vip',
        'empresa_id',
        'data_expiracao',
        'url',
    ];
}
