<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transportadora extends Model
{
    use HasFactory, SoftDeletes;

    protected $hidden = [
        'deleted_at',
        'updated_at',
        'created_at',
        'id_usuario',
    ];

    protected $fillable = [
        "cnpj",
        "razao_social",
        "endereco_comercial",
        "cidade",
        "estado",
        "cep",
        "telefone",
        "celular",
        "logo",
        "id_usuario",
        "nome_funcionario",
        "cargo"
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
