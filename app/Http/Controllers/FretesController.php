<?php

namespace App\Http\Controllers;

use App\Models\Frete;
use App\Models\Transportadora;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FretesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user = auth()->user();
            // SELECT *, t.razao_social FROM fretes AS f JOIN transportadoras AS t ON f.empresa_id = t.id_usuario

            switch ($user->tipo_usuario) {
                case 'Administrador':
                    $fretes = Frete::where('fretes.deleted_at', null)
                        ->where('fretes.data_expiracao', '>=', date('Y-m-d'))
                        ->where('fretes.codigo_frete', '<>', "")
                        ->join("transportadoras", "transportadoras.id_usuario", "=", "fretes.empresa_id")
                        ->select('fretes.*', 'transportadoras.razao_social')
                        ->get();
                    break;
                case 'Caminhoneiro':
                    $fretes = Frete::where('fretes.deleted_at', null)
                        ->where('fretes.data_expiracao', '>=', date('Y-m-d'))
                        ->where('fretes.codigo_frete', '<>', "")
                        ->where('fretes.frete_aprovado', true)
                        ->join("transportadoras", "transportadoras.id_usuario", "=", "fretes.empresa_id")
                        ->select('fretes.*', 'transportadoras.razao_social')
                        ->get();
                    break;
                case 'Transportadora':
                    $fretes = Frete::where('fretes.deleted_at', null)
                        ->where('fretes.data_expiracao', '>=', date('Y-m-d'))
                        ->where('fretes.codigo_frete', '<>', "")
                        ->where('fretes.empresa_id', $user->id)
                        ->join("transportadoras", "transportadoras.id_usuario", "=", "fretes.empresa_id")
                        ->select('fretes.*', 'transportadoras.razao_social')
                        ->get();
                    break;
                case 'Anunciante':
                    return response()->json(['mensagem' => 'Este usuário não tem permissão para isto.'], 401);
                    break;
                default:
                    return response()->json(['mensagem' => 'Não foi possível fazer a listagem de fretes, pois o tipo de usuário não foi encontrado.'], 400);
                    break;
            }

            return response()->json(['fretes' => $fretes], 200);
        } catch (\Throwable $th) {
            return response()->json(['mensagem' => 'Não foi possível fazer a listagem de fretes.', 'stack' => $th], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = auth()->user();

            if ($user->tipo_usuario != 'Transportadora') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para criar fretes.'], 401);
            }

            $date = date("Y-m-d");
            $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));

            switch ($request->dataDias) {
                case 30:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 30 days'));
                    break;
                case 90:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 90 days'));
                    break;
                case 360:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 360 days'));
                    break;
                default:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 30 days'));
                    break;
            }

            $transportadora = Transportadora::where('id_usuario', $user->id)->first();

            $frete = Frete::create([
                "logo" => $transportadora->logo,
                "tipo_veiculo" => $request->tipo_veiculo,
                "tipo_carreta" => $request->tipo_carreta,
                "tipo_carroceria" => $request->tipo_carroceria,
                "dimensoes_minimas" => $request->dimensoes_minimas,
                "requisitos_extras" => $request->requisitos_extras,
                "tipo_produto" => $request->tipo_produto,
                "acondicionamento_produto" => $request->acondicionamento_produto,
                "nome_responsavel_coleta" => $request->nome_responsavel_coleta,
                "data_coleta" => $request->data_coleta,
                "celular_coleta" => $request->celular_coleta,
                "endereco_coleta" => $request->endereco_coleta,
                "cidade_coleta" => $request->cidade_coleta,
                "estado_coleta" => $request->estado_coleta,
                "valor_frete" => $request->valor_frete,
                "peso_toneladas" => $request->peso_toneladas,
                "nome_responsavel_entrega" => $request->nome_responsavel_entrega,
                "data_entrega" => $request->data_entrega,
                "celular_entrega" => $request->celular_entrega,
                "endereco_entrega" => $request->endereco_entrega,
                "cidade_entrega" => $request->cidade_entrega,
                "estado_entrega" => $request->estado_entrega,
                "tempo_dias" => $request->dataDias,
                "data_expiracao" => $expirationDate,
                "frete_aprovado" => false,
                "empresa_id" => $user->id,
                "codigo_frete" => Str::uuid(),
            ]);
            return response()->json(['mensagem' => 'Frete salvo com sucesso.', 'frete' => $frete], 201);
        } catch (Exception $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o frete.', 'stack' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = auth()->user();

            if ($user->tipo_usuario != 'Administrador') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar este frete.'], 401);
            }

            $frete = Frete::where('id', $id)->first();

            if ($frete == null) {
                return response()->json(['mensagem' => 'Frete não encontrado.'], 400);
            }

            $frete->update(["frete_aprovado" => $request->frete_aprovado]);

            return response()->json(['mensagem' => 'Frete atualizado com sucesso.', 'frete' => $frete], 200);
        } catch (Exception $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o frete.', 'stack' => $th], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = auth()->user();

            if ($user->tipo_usuario == 'Caminhoneiro' || $user->tipo_usuario == 'Anunciante') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar este frete.'], 401);
            }

            $todosFrete = Frete::where('id', $id)->get();

            if ($todosFrete == null) {
                return response()->json(['mensagem' => 'Frete não encontrado.'], 400);
            }

            if ($user->tipo_usuario == 'Administrador') {
                $todosFrete->first()->delete();
                return response()->json(['mensagem' => 'Frete atualizado com sucesso.'], 200);
            }

            $frete = $todosFrete->where('empresa_id', $user->id)->first();

            if ($frete == null) {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar esse frete.'], 400);
            }

            $frete->delete();

            return response()->json(['mensagem' => 'Frete atualizado com sucesso.'], 200);
        } catch (Exception $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o frete.', 'stack' => $th], 400);
        }
    }
}
