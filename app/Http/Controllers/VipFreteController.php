<?php

namespace App\Http\Controllers;

use App\Models\Publicidade;
use Illuminate\Http\Request;

class VipFreteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user = auth()->user();

            switch ($user->tipo_usuario) {
                case 'Administrador':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Caminhoneiro':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->where('publicidade_aprovada', true)
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Anunciante':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->where('empresa_id', $user->id)
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Transportadora':
                    return response()->json(['mensagem' => 'Este usuário não tem permissão para isto.'], 401);
                    break;
                default:
                    return response()->json(['mensagem' => 'Não foi possível fazer a listagem de publicidade, pois o tipo de usuário não foi encontrado.'], 400);
                    break;
            }

            return response()->json(['publicidades' => $publicidades], 200);
        } catch (\Throwable $th) {
            return response()->json(['mensagem' => 'Não foi possível fazer a listagem de publicidade.', 'stack' => $th], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
