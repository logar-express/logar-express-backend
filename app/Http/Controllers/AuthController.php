<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Mail\SendMailResetPassword;
use App\Models\Caminhoneiro;
use App\Models\Transportadora;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'cadastro', 'resetpassword']]);
    }

    public function resetpassword(Request $request)
    {
        try {
            if (!$request->email) {
                return response()->json(['mensagem' => 'É necessário informar o email para alterar a senha.'], 400);
            }

            $user = User::where('email', $request->email)->first();

            if (!$user) {
                return response()->json(['mensagem' => 'O usuário não foi encontrado para esse email.'], 400);
            }

            $senha = $this->randomPassword();
            $user->update(["password" => bcrypt($senha)]);
            Mail::to($request->email)->send(new SendMailResetPassword($senha));

            return response()->json(['mensagem' => 'Senha alterada com sucesso. Você a receberá no seu email.'], 201);
        } catch (Exception $e) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao resetar a senha.', 'stack' => $e], 400);
        }
    }

    public function cadastro(Request $request)
    {
        try {
            $user = User::create([
                "tipo_usuario" => 'Anunciante',
                "name" => $request->nome,
                "email" => $request->email,
                "password" => bcrypt($request->password)
            ]);

            return response()->json(['mensagem' => 'Usuário criado com sucesso.', 'user' => $user], 201);
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json(['mensagem' => 'O email já existe.', 'stack' => $e], 400);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        try {
            $credentials = request(['email', 'password']);

            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['mensagem' => 'Unauthorized'], 401);
            }

            return $this->respondWithToken($token);
        } catch (\Throwable $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao fazer login.', 'error' => $th], 400);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        try {
            $user = auth()->user();

            switch ($user->tipo_usuario) {
                case "Caminhoneiro":
                    $dados = Caminhoneiro::where('id_usuario', $user->id)->first();
                    return response()->json(['user' => $user, 'dados' => $dados]);
                    break;
                case "Transportadora":
                    $dados = Transportadora::where('id_usuario', $user->id)->first();
                    return response()->json(['user' => $user, 'dados' => $dados]);
                    break;
                default:
                    return response()->json(['user' => $user]);
                    break;
            }
        } catch (\Throwable $th) {
            return response()->json(['mensagem' => 'Houve um erro ao carregar os dados do usuário.', 'error' => $th], 400);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['mensagem' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $user = auth()->user();

        switch ($user->tipo_usuario) {
            case "Caminhoneiro":
                $dados = Caminhoneiro::where('id_usuario', $user->id)->first();
                // return response()->json(['user' => $user, 'dados' => $dados]);
                break;
            case "Transportadora":
                $dados = Transportadora::where('id_usuario', $user->id)->first();
                // return response()->json(['user' => $user, 'dados' => $dados]);
                break;
            default:
                $dados = null;
        }

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'hora_expiracao' => date(DATE_ATOM, mktime(date("H") + 1, date("i"), date("s"), date("m"), date("d"), date("Y"))),
            'expires_in' => auth()->factory()->getTTL() * 60,
            'dados' => $dados,
            'role' => $user->tipo_usuario
        ]);
    }

    protected function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
