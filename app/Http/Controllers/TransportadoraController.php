<?php

namespace App\Http\Controllers;

use App\Models\Transportadora;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class TransportadoraController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = User::create([
                "tipo_usuario" => 'Transportadora',
                "name" => $request->name,
                "email" => $request->email,
                "password" => bcrypt($request->password)
            ]);

            $transportadora = Transportadora::create([
                "cnpj" => $request->cnpj,
                "razao_social" => $request->razao_social,
                "endereco_comercial" => $request->endereco_comercial,
                "cidade" => $request->cidade,
                "estado" => $request->estado,
                "cep" => $request->cep,
                "telefone" => $request->telefone,
                "celular" => $request->celular,
                "logo" => $request->logo,
                "id_usuario" => $user->id,
                "nome_funcionario" => $request->nome_funcionario,
                "cargo" => $request->cargo
            ]);

            return response()->json(['mensagem' => 'Usuário criado com sucesso.', 'transportadora' => $transportadora], 201);
        } catch (Exception $e) {
            $user->delete();
            return response()->json(['mensagem' => 'O email ou o cnpj já existe.', 'stack' => $e], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
