<?php

namespace App\Http\Controllers;

use App\Models\Caminhoneiro;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class CaminhoneiroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $user = User::where('email', $request->email)->first();

            if ($user != null) {
                return response()->json(['mensagem' => 'O email já existe.'], 400);
            }

            $caminhoneiro = Caminhoneiro::where('cpf', $request->cpf)->first();

            if ($caminhoneiro != null) {
                return response()->json(['mensagem' => 'O CPF já existe.'], 400);
            }

            $caminhoneiro = Caminhoneiro::where('cnh', $request->cnh)->first();

            if ($caminhoneiro != null) {
                return response()->json(['mensagem' => 'A CNH já está cadastrada.'], 400);
            }

            $user = User::create([
                "tipo_usuario" => 'Caminhoneiro',
                "name" => $request->name,
                "email" => $request->email,
                "password" => bcrypt($request->password)
            ]);

            $caminhoneiro = Caminhoneiro::create([
                "nome" => $request->name,
                "cpf" => $request->cpf,
                "cnh" => $request->cnh,
                "data_emissao_cnh" => $request->data_emissao_cnh,
                "estado_expeditor_cnh" => $request->estado_expeditor_cnh,
                "telefone" => $request->telefone,
                "celular" => $request->celular,
                "possui_mopp" => $request->possui_mopp,
                "marca" => $request->marca,
                "modelo" => $request->modelo,
                "cor" => $request->cor,
                "placa" => $request->placa,
                "ano" => $request->ano,
                "renavam" => $request->renavam,
                "tipo_veiculo" => $request->tipo_veiculo,
                "tipo_carreta" => $request->tipo_carreta,
                "tipo_carroceria" => $request->tipo_carroceria,
                "id_usuario" => $user->id,
            ]);

            return response()->json(['mensagem' => 'Usuário criado com sucesso.', 'caminhoneiro' => $caminhoneiro], 201);
        } catch (Exception $e) {
            if ($user) {
                $user->delete();
            }
            return response()->json(['mensagem' => 'Houve um erro e não foi possível cadastrar o usuário.', 'stack' => $e], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
