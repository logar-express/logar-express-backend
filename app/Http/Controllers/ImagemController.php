<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagemController extends Controller
{
    public function uploadImagem(Request $request)
    {
        $uploadFolder = 'anuncios';
        $image = $request->file('file');
        $image_uploaded_path = $image->store($uploadFolder, 'public');
        $uploadedImageResponse = array(
            "image_name" => basename($image_uploaded_path),
            "image_url" => Storage::disk('public')->url($image_uploaded_path),
            "mime" => $image->getClientMimeType()
        );

        return response()->json(['mensagem' => 'Imagem carregada com sucesso.', 'uploadedImageResponse' => $uploadedImageResponse], 201);
    }

    public function retrieveImage($filename)
    {
        return Storage::disk('public')->download('anuncios/' . $filename);
    }
}
