<?php

namespace App\Http\Controllers;

use App\Models\Publicidade;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Throwable;

class PublicidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user = auth()->user();

            switch ($user->tipo_usuario) {
                case 'Administrador':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', '<>', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Caminhoneiro':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', '<>', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->where('publicidade_aprovada', true)
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Anunciante':
                    $publicidades = Publicidade::where('deleted_at', null)
                        ->where('data_expiracao', '>=', date('Y-m-d'))
                        ->where('tempo_dias', '<>', 30)
                        ->where('codigo_publicidade', '<>', "")
                        ->where('empresa_id', $user->id)
                        ->join("users", "users.id", "=", "publicidades.empresa_id")
                        ->select('publicidades.*', 'users.name')
                        ->get();
                    break;
                case 'Transportadora':
                    return response()->json(['mensagem' => 'Este usuário não tem permissão para isto.'], 401);
                    break;
                default:
                    return response()->json(['mensagem' => 'Não foi possível fazer a listagem de publicidade, pois o tipo de usuário não foi encontrado.'], 400);
                    break;
            }

            return response()->json(['publicidades' => $publicidades], 200);
        } catch (\Throwable $th) {
            return response()->json(['mensagem' => 'Não foi possível fazer a listagem de publicidade.', 'stack' => $th], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $user = auth()->user();

            if ($user->tipo_usuario != 'Anunciante') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para criar fretes.'], 401);
            }

            $date = date("Y-m-d");
            $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));

            switch ($request->tempo_dias) {
                case 10:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));
                    break;
                case 20:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 20 days'));
                    break;
                case 30:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 30 days'));
                    break;
                default:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));
                    break;
            }

            $publicidade = Publicidade::create([
                "titulo" => $request->titulo,
                "segmento" => $request->segmento,
                "preco" => $request->preco,
                "descricao" => $request->descricao,
                "imagem_vip" => $request->imagem_vip,
                "tempo_dias" => $request->tempo_dias,
                "caminho_arquivo" => $request->caminho_arquivo,
                "codigo_publicidade" => Str::uuid(),
                "publicidade_aprovada" => false,
                "empresa_id" => $user->id,
                "url" => $request->url,
                "data_expiracao" => $expirationDate
            ]);

            return response()->json(['mensagem' => 'Publicidade salvo com sucesso.', 'publicidade' => $publicidade], 201);
        } catch (Throwable $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o anúncio.', 'stack' => $th], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = auth()->user();

            if ($user->tipo_usuario != 'Administrador') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar este frete.'], 401);
            }

            $publicidade = Publicidade::where('id', $id)->first();

            if ($publicidade == null) {
                return response()->json(['mensagem' => 'Anuncio não encontrado.'], 400);
            }

            $date = date("Y-m-d");
            $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));

            switch ($publicidade->tempo_dias) {
                case 10:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));
                    break;
                case 20:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 20 days'));
                    break;
                case 30:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 30 days'));
                    break;
                default:
                    $expirationDate = date('Y-m-d', strtotime($date . ' + 10 days'));
                    break;
            }

            $publicidade->update(["publicidade_aprovada" => $request->publicidade_aprovada, "data_expiracao" => $expirationDate]);

            return response()->json(['mensagem' => 'Anuncio atualizado com sucesso.', 'publicidade' => $publicidade], 200);
        } catch (Exception $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o anuncio.', 'stack' => $th], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = auth()->user();

            if ($user->tipo_usuario == 'Caminhoneiro' || $user->tipo_usuario == 'Transportadora') {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar este anuncio.'], 401);
            }

            $todosAnuncios = Publicidade::where('id', $id)->get();

            if ($todosAnuncios == null) {
                return response()->json(['mensagem' => 'Anuncio não encontrado.'], 400);
            }

            if ($user->tipo_usuario == 'Administrador') {
                $todosAnuncios->first()->delete();
                return response()->json(['mensagem' => 'Anuncio atualizado com sucesso.'], 200);
            }

            $anuncio = $todosAnuncios->where('empresa_id', $user->id)->first();

            if ($anuncio == null) {
                return response()->json(['mensagem' => 'Este usuário não tem permissão para editar esse anuncio.'], 400);
            }

            $anuncio->delete();

            return response()->json(['mensagem' => 'Anuncio atualizado com sucesso.'], 200);
        } catch (Exception $th) {
            return response()->json(['mensagem' => 'Ocorreu um erro ao salvar o anuncio.', 'stack' => $th], 400);
        }
    }
}
