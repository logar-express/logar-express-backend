<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailResetPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $senha;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($senha)
    {
        $this->senha = $senha;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('nao-responda@logarexpress.com')
            ->markdown('emails.reset-password')
            ->subject("Troca de senha - LogarExpress")
            ->with(['senha' => $this->senha]);
    }
}
