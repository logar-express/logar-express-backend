<html>
    <body>
        <p>Olá!</p>
        <p></p>
        <p>A sua senha foi atualizada para: <b>{{ $senha }}</b></p>
        <p></p>
        <p><small>Não responda a este email.</small></p>
        <p>Att, <br>
        Equipe Logar Express!</p>
    </body>
</html>
