<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'O :attribute deve ser aceito.',
    'active_url' => 'O :attribute não é uma URL válida.',
    'after' => 'O :attribute deve ser uma data após :date.',
    'after_or_equal' => 'O :attribute deve ser uma data igual ou após :date.',
    'alpha' => 'O :attribute deve conter apenas letras.',
    'alpha_dash' => 'O :attribute deve conter apenas letras e números.',
    'alpha_num' => 'O :attribute deve conter apenas letras e números.',
    'array' => 'O :attribute deve ser uma listagem.',
    'before' => 'O :attribute deve ser uma data antes de :date.',
    'before_or_equal' => 'O :attribute deve ser uma data antes de ou igual a :date.',
    'between' => [
        'numeric' => 'O :attribute deve estar entre :min e :max.',
        'file' => 'O :attribute deve estar entre :min e :max kilobytes.',
        'string' => 'O :attribute deve estar entre :min e :max caracteres.',
        'array' => 'O :attribute must have between :min e :max itens.',
    ],
    'boolean' => 'O :attribute o campo deve ser verdadeiro ou falso.',
    'confirmed' => 'O :attribute confirmação não é válida.',
    'current_password' => 'A senha está incorreta.',
    'date' => 'O :attribute não é uma data válida.',
    'date_equals' => 'O :attribute deve ser uma data igual a :date.',
    'date_format' => 'O :attribute não bate com o formato :format.',
    'different' => 'O :attribute e :other devem ser diferentes.',
    'digits' => 'O :attribute deve ser :digits digitos.',
    'digits_between' => 'O :attribute deve estar entre :min e :max digitos.',
    'dimensions' => 'As :attribute são inválidas.',
    'distinct' => 'O :attribute já existe.',
    'email' => 'O :attribute deve ser um endereço de email válido.',
    'ends_with' => 'O :attribute deve encerrar com um dos seguintes valores: :values.',
    'exists' => 'O :attribute selecionado é inválido.',
    'file' => 'O :attribute deve ser um arquivo.',
    'filled' => 'O :attribute deve ser preenchido obrigatoriamente.',
    'gt' => [
        'numeric' => 'O :attribute deve ser maior que :value.',
        'file' => 'O :attribute deve ser maior que :value kilobytes.',
        'string' => 'O :attribute deve ser maior que :value characteres.',
        'array' => 'O :attribute deve ter mais que :value itens.',
    ],
    'gte' => [
        'numeric' => 'O :attribute deve ser maior que ou igual a :value.',
        'file' => 'O :attribute deve ser maior que ou igual a :value kilobytes.',
        'string' => 'O :attribute deve ser maior que ou igual a :value characteres.',
        'array' => 'O :attribute deve ter :value itens ou mais.',
    ],
    'image' => 'O :attribute deve ser uma imagem.',
    'in' => 'O :attribute selecionado é inválido.',
    'in_array' => 'O :attribute não existe em :other.',
    'integer' => 'O :attribute deve ser um número inteiro.',
    'ip' => 'O :attribute deve ser endereço de IP válido.',
    'ipv4' => 'O :attribute deve ser endereço de IPv4 válido.',
    'ipv6' => 'O :attribute deve ser endereço de IPv6 válido.',
    'json' => 'O :attribute deve ser um JSON válido.',
    'lt' => [
        'numeric' => 'O :attribute deve ser menor que :value.',
        'file' => 'O :attribute deve ser menor que :value kilobytes.',
        'string' => 'O :attribute deve ser menor que :value characteres.',
        'array' => 'O :attribute deve ser menor que :value itens.',
    ],
    'lte' => [
        'numeric' => 'O :attribute deve ser menor que ou igual a :value.',
        'file' => 'O :attribute deve ser menor que ou igual a :value kilobytes.',
        'string' => 'O :attribute deve ser menor que ou igual a :value characteres.',
        'array' => 'O :attribute não deve ser maior que :value itens.',
    ],
    'max' => [
        'numeric' => 'O :attribute must not be maior que :max.',
        'file' => 'O :attribute must not be maior que :max kilobytes.',
        'string' => 'O :attribute must not be maior que :max characteres.',
        'array' => 'O :attribute não deve ser maior que :max itens.',
    ],
    'mimes' => 'O :attribute deve ser um arquivo do tipo: :values.',
    'mimetypes' => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min' => [
        'numeric' => 'O :attribute deve ter pelo menos :min.',
        'file' => 'O :attribute deve ter pelo menos :min kilobytes.',
        'string' => 'O :attribute deve ter pelo menos :min characteres.',
        'array' => 'O :attribute deve ter pelo menos :min itens.',
    ],
    'multiple_of' => 'O :attribute deve ser um múltiplo de :value.',
    'not_in' => 'O selected :attribute é inválido.',
    'not_regex' => 'O :attribute format é inválido.',
    'numeric' => 'O :attribute deve ser um número.',
    'password' => 'A senha está incorreta.',
    'present' => 'O campo :attribute deve ser preenchido.',
    'regex' => 'O :attribute formato é inválido.',
    'required' => 'O :attribute é obrigatório.',
    'required_if' => 'O :attribute é obrigatório quando :other é :value.',
    'required_unless' => 'O :attribute é obrigatório a menos que :other está em :values.',
    'required_with' => 'O :attribute é obrigatório quando :values está preenchido.',
    'required_with_all' => 'O :attribute é obrigatório quando :values estão preenchidos.',
    'required_without' => 'O :attribute é obrigatório quando :values não está preenchido.',
    'required_without_all' => 'O :attribute é obrigatório quando nenhum dos :values estão preenchidos.',
    'prohibited' => 'O :attribute é proibido.',
    'prohibited_if' => 'O :attribute é proibido quando :other é :value.',
    'prohibited_unless' => 'O :attribute é proibido a menos que :other está em :values.',
    'same' => 'O :attribute e :other devem ser iguais.',
    'size' => [
        'numeric' => 'O :attribute deve ser :size.',
        'file' => 'O :attribute deve ser :size kilobytes.',
        'string' => 'O :attribute deve ser :size characteres.',
        'array' => 'O :attribute deve conter :size itens.',
    ],
    'starts_with' => 'O :attribute deve começar com um dos valores: :values.',
    'string' => 'O :attribute deve ser uma palavra ou frase.',
    'timezone' => 'A :attribute deve ser válida.',
    'unique' => 'O :attribute já existe.',
    'uploaded' => 'O :attribute falhou ao fazer o upload.',
    'url' => 'O :attribute deve ser uma URL válido.',
    'uuid' => 'O :attribute deve ser uma UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
