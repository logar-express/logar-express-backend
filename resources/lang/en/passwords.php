<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sua senha foi resetada com sucess.',
    'sent' => 'Nós enviamos um email com o link para resetar sua senha.',
    'throttled' => 'Por favor espere antes de tentar novamente.',
    'token' => 'O token para resetar a senha está inválido.',
    'user' => "Não foi possível encontrar este email no nosso banco de dados.",

];
