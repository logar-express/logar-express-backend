<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/teste', function () {
    $email = "glaydson.prado@gmail.com";
    $mailid = $email;
    $subject = 'News Information.';
    $data = array('email' => $mailid, 'subject' => $subject);
    Mail::send('emails.reset-password', $data, function ($message) use ($data) {
        $message->from('nao-responda@logarexpress.com', 'News Information');
        $message->to("glaydson.prado@gmail.com");
        $message->subject("teste email");
    });
    return 'ok';
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('cadastro', [App\Http\Controllers\AuthController::class, 'cadastro']);
    Route::post('login', [App\Http\Controllers\AuthController::class, 'login']);
    Route::post('logout', [App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('refresh', [App\Http\Controllers\AuthController::class, 'refresh']);
    Route::post('me', [App\Http\Controllers\AuthController::class, 'me']);
    Route::post('reset-password', [App\Http\Controllers\AuthController::class, 'resetpassword']);
});

Route::apiResources([
    'transportadora' => App\Http\Controllers\TransportadoraController::class,
    'caminhoneiro' => App\Http\Controllers\CaminhoneiroController::class,
    'frete' => App\Http\Controllers\FretesController::class,
    'publicidade' => App\Http\Controllers\PublicidadeController::class,
    'publicidade-vip' => App\Http\Controllers\VipFreteController::class,
]);

Route::post('imagem', [App\Http\Controllers\ImagemController::class, 'uploadImagem']);
Route::get('imagem/{filename}', [App\Http\Controllers\ImagemController::class, 'retrieveImage']);
